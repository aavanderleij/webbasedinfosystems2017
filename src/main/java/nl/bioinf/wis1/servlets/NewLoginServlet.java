package nl.bioinf.wis1.servlets;

import nl.bioinf.wis1.users.Address;
import nl.bioinf.wis1.users.User;

import javax.security.auth.login.LoginException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "NewLoginServlet", urlPatterns = "/newLogin.do")
public class NewLoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        if (session.isNew() || session.getAttribute("user") == null) {
            try {
                User u = loginUser(username, password);
                session.setAttribute("user", u);
            } catch (LoginException e) {
                request.setAttribute("login_error", e.getMessage());
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        dispatcher.forward(request, response);
    }

    private User loginUser(String username, String password) throws LoginException {
        if (username == null || password == null) {
            throw new LoginException("no username and/or password provided");
        } else {
            if (username.equalsIgnoreCase("Henk") && password.equalsIgnoreCase("Henk")) {
                User u = new User(username, password);
                u.setEmail("Henk@example.com");
                u.setAddress(new Address(1, "90210", "Beverly hills", "Beverly hills"));
                return u;
            }
            throw new LoginException("wrong username and/or password");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        dispatcher.forward(request, response);
    }
}
