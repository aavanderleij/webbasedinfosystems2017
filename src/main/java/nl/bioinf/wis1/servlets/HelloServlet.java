package nl.bioinf.wis1.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

@WebServlet(name = "HelloServlet", urlPatterns = "/hallo")
public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LocalDateTime time = LocalDateTime.now();
        request.setAttribute("time", time.toString());
        request.setAttribute("name", "Michiel");
        RequestDispatcher view = request.getRequestDispatcher("hello.jsp");
        view.forward(request, response);
//        PrintWriter out = response.getWriter();
//        out.write("<html><head><title>MyFirstServlet</title></head><body><h1>Hello, it is now "
//                + time.toString()
//                + "</h1></body></html>");
    }
}
