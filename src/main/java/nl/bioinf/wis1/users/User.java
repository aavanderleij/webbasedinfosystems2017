package nl.bioinf.wis1.users;

public class User {
    private String theUserName;
    private String passWord;
    private String email;
    private Address address;

    public User(String userName, String passWord) {
        this.theUserName = userName;
        this.passWord = passWord;
    }

    public String getUserName() {
        return theUserName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "theUserName='" + theUserName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }
}
