<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 28-11-17
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>My Users</title>
</head>
<body>
    <h2>User: ${requestScope.userOne.userName}</h2>
    <h2>Pass: ${requestScope.userOne.passWord}</h2>
    <h2>address: ${requestScope.userOne.address.street} ${requestScope.userOne.address.number}</h2>
    <h2>email: ${requestScope.userOne.email}</h2>

    <h1>All registered users</h1>
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>email</th>
                <th>address</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="person" items="${requestScope.registered_users}">
                <tr>
                    <td>${person.userName}</td>
                    <td>${person.email}</td>
                    <td>${person.address.printableAddress}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <br />

    <jsp:include page="includes/footer.jsp">
        <jsp:param name="todays_saying" value="Beter een dode kakkerlak in de tuin dan een levende in de keuken!" />
    </jsp:include>
</body>
</html>
