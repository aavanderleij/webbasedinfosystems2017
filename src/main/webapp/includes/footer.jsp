<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 29-11-17
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<div>
    Current user: ${requestScope.user}
    Copyright (C) 2017 Admin Piet ${initParam.admin_email}.
    Today's saying is ${param.todays_saying}

    <a href="<c:url value = "/Login.jsp"/>" >go to login page</a>
</div>
